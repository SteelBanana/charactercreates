package main;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.List;

public class Input {
	static BufferedReader br=new BufferedReader(new InputStreamReader(System.in));

	//文字列入力
	public static String inputString(){
		String str=new String();
		try{
			str=br.readLine();
		}catch(IOException e){
			System.out.println(e.getMessage());
		}
		return str;
	}

	//自然数入力
	public static int inputNum(){
		int num=-1;
		String str=new String();
		while(true){
			try{
				str=br.readLine();
			}catch(IOException e){
				System.out.println(e.getMessage());
			}
			try{
				num=Integer.parseInt(str);
			}catch(NumberFormatException e){
				System.out.print("再入力>>>");
				continue;
			}
			if(num<0){
				continue;
			}
			return num;
		}
	}
	//自然数入力(上限設定)
	public static int inputNum(int max){
		int num=-1;
		String str=new String();
		while(true){
			try{
				str=br.readLine();
			}catch(IOException e){
				System.out.println(e.getMessage());
			}
			try{
				num=Integer.parseInt(str);
			}catch(NumberFormatException e){
				System.out.print("再入力>>>");
				continue;
			}
			if(num<0 ||num>max){
				System.out.print("再入力>>>");
				continue;
			}
			return num;
		}
	}

	//配列要素数範囲の数字入力
	public static <T> int inputOutofBounse(List<T> list){
		while(true){
			int num=inputNum();
			if(0<num && num<=list.size()){
				return num-1;
			}else if(num==0){
				return -1;
			}else{
				System.out.println("存在しません!");
				System.out.print("再入力>>>");
			}
		}
	}

	public static boolean yesOrNo(){
		System.out.print("承諾(y),拒否(n)>>>>>");
		while(true){
			String yn=inputString();
			if(yn.equals("y")){
				return true;
			}else if(yn.equals("n")){
				return false;
			}else{
				System.out.print("再入力>");
			}
		}
	}



}