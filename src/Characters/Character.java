package Characters;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Random;

import Equipments.Equipment;

public class Character {

	String name;//名前
	private int hp;//基礎HP
	private int Attack;//基礎攻撃
	private int heal;//基礎回復
	static private  List<String> EllementName=Arrays.asList("無","火","水","光","闇");//属性実名リスト
	private String Ellement;//属性実名
	private int HpBonus;//HPボーナス
	private int attackBonus;//攻撃ボーナス
	private int healBonus;//回復ボーナス
	private static final int MaxTotalBonus=30;//ボーナス上限
	private int totalBonus;//ボーナス合計
	private static int remnantBonus=0;//残りボーナス値
	private static List<Character> Characters=new ArrayList<>();//キャラリスト
	private Equipment equ;//装備しているアイテム
	private boolean hasEqu=false;//装備管理フラグ

	//キャラ名
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}

	//HP系
	//基礎値
	public int getbaseHp() {
		return hp;
	}
	//基礎+ボーナス+装備
	public int getHp(){
		int hp=getbaseHp()+getHpBonus()*10;
		if(this.equ!=null){
			hp+=this.equ.getHp();
		}
		return hp;
	}
	public void setBaseHp(int hp) {
		this.hp = hp;
	}

	//攻撃系
	//基礎値
	public int getBaseAttack() {
		return Attack;
	}
	//基礎+ボーナス+装備
	public int getAttack(){
		int attack=getBaseAttack()+getAttackBonus()*5;
		if(this.equ!=null){
			attack+=this.equ.getAttack();
		}
		return attack;
	}
	public void setBaseAttack(int attack) {
		Attack = attack;
	}

	//回復系
	//基礎
	public int getBaseHeal() {
		return heal;
	}
	//基礎+ボーナス+装備
	public int getHeal(){
		int heal=getBaseHeal()+getHealBonus()*3;
		if(this.equ!=null){
			heal+=this.equ.getHeal();
		}
		return heal;
	}
	public void setBaseHeal(int heal) {
		this.heal = heal;
	}

	//属性系
	//属性要素数取得
	public int getEllement(Character chara) {
		return EllementName.indexOf(chara.getEllement());
	}
	//属性名取得
	public String getEllement(){
		return Ellement;
	}
	//属性名直接取得
	public static String getEllement(int ellement){
		return EllementName.get(ellement);
	}
	public void setEllement(int ellement) {
		Ellement = EllementName.get(ellement);
	}

	//ボーナス系
	public int getHpBonus() {
		return HpBonus;
	}
	public void setHpBonus(int hpBonus) {
		HpBonus = hpBonus;
	}
	public int getAttackBonus() {
		return attackBonus;
	}
	public void setAttackBonus(int attackBonus) {
		this.attackBonus = attackBonus;
	}
	public int getHealBonus() {
		return healBonus;
	}
	public void setHealBonus(int healBonus) {
		this.healBonus = healBonus;
	}
	public static int getRemnantBonus() {
		return remnantBonus;
	}
	public static void addRemnantBonus(int addBonus) {
		remnantBonus += addBonus;
	}
	public static void subRemnantBonus(int subBonus){
		remnantBonus-=subBonus;
	}
	public int getTotalBonus() {
		return totalBonus;
	}
	public void setTotalBonus(int totalBonus){
		this.totalBonus=totalBonus;
	}
	//ボーナス最大値取得
	public static int getMaxBonus(){
		return MaxTotalBonus;
	}

	//装備系
	public Equipment getEqu() {
		return equ;
	}
	public void setEqu(Equipment equ) {
		this.equ = equ;
		this.hasEqu=true;
		equ.setableEqu(false);
		equ.setSettingCharacter(this);
	}

	public void unsetEqu(){
		this.equ=null;
		this.hasEqu=false;
	}

	public boolean getHasEqu(){
		return hasEqu;
	}

	public String getEquName(){
		if(this.equ!=null){
			return this.getEqu().getName();
		}else{
			return "なし";
		}
	}

	//その他
	//キャラクター総数取得
	public static int getNumberOfCharacters(){
		return getCharacters().size();
	}
	//キャラクター取得
	public static Character getCharacter(int num){
		return getCharacters().get(num);
	}
	//キャラList取得
	public static List<Character> getCharacters() {
		return Characters;
	}

	//デバッグ用自動生成
	public static void DebugCharacters(){
		for(int i=0; i<20;i++){
			Character chara=new Character();
			String name="Debug"+(i+1);
			//各ステータス乱数生成
			int bonus=new Random().nextInt(21)+15;
			int hpAddRandom=new Random().nextInt(11)+115;
			int attackAddRandom=new Random().nextInt(11)+15;
			int healAddRandom=new Random().nextInt(11)+15;
			int ellementRandom=i%5;
			chara=CharacterCreates.charaMaker(name,hpAddRandom,attackAddRandom
					,healAddRandom,ellementRandom,bonus);
			}
	}

}
