package Equipments;

import java.util.ArrayList;
import java.util.List;

import Characters.Character;
import Characters.CharacterDisplay;
import main.Input;

public class EquipmentFixes {

	//装備確定後キャラ選択(装備詳細画面呼び出し)
	public static void setEqu(Equipment equ){
		System.out.println();
		setEqu:while(CharacterDisplay.hasCharaList()){
			List<Character> setableChara=new ArrayList<>();//仮リスト定義
			System.out.println("装備可能なキャラクターを表示します");
			for(Character chara:Character.getCharacters()){
				if(equ.getEllement().equals(chara.getEllement())){
					//装備可能ならadd
					setableChara.add(chara);
				}
			}
			for(Character chara:setableChara){
				//装備の装備キャラが一致&&キャラの装備一致
				if(equ.getSettingCharacter()==chara && chara.getEqu()==equ){
					System.out.print("[装備中]");
				}
				CharacterDisplay.charaSimpleDisp(chara,setableChara);
			}
			if(setableChara.isEmpty()==false){
			System.out.print("装備させたいキャラクターNo.を入力>>>");
				while(true){
					int setnum=Input.inputOutofBounse(setableChara);
					if(setnum<0){
						break setEqu;//0バック
					}else{
						//装備実メソッド
						setEqu(setableChara.get(setnum),equ);
						break setEqu;
					}
				}
			}else{
				System.out.println("装備可能なキャラクターがいません");
				break setEqu;
			}
		}
	}
	//キャラ確定後装備選択(キャラ画面呼び出し)
	public static void setEqu(Character chara){
		System.out.println();
		setEqu:while(EquipmentDisplay.hasEquipment()){
			List<Equipment> setableEqu=new ArrayList<>();
			System.out.println("装備可能な装備を表示します");
			for(Equipment equ:Equipment.getEquipments()){
				//属性一致
				if(chara.getEllement().equals(equ.getEllement())){
					setableEqu.add(equ);
				}
			}

			for(Equipment equ:setableEqu){
				if(equ.getSettingCharacter()==chara && chara.getEqu()==equ){
					System.out.print("[装備中]");
				}
				EquipmentDisplay.equSimpleDisp(equ, setableEqu);
			}
			//装備可能があるばあい
			if(setableEqu.isEmpty()==false){
			System.out.println("装備させたいアイテムを選択");
				while(true){
					int setnum=Input.inputOutofBounse(setableEqu);
					if(setnum<0){
						break setEqu;
					}else{
						//装備実メソッド
						setEqu(chara,setableEqu.get(setnum));
						break setEqu;
					}
				}
			}else{
				//装備可能アイテムがないとき
				System.out.println("装備可能なアイテムがありません");
				break setEqu;
			}
		}
	}
	//装備メニューから呼び出し
	public static void setEqu(){
		System.out.println();
		System.out.println("装備一覧");
		EquipmentDisplay.equSimpleList();//装備アイテム全表示
		System.out.println("装備させたいアイテムを選択");
		while(true){
			int setnum=Input.inputOutofBounse(Equipment.getEquipments());
			if(setnum<0){
				break;
			}else{
				setEqu(Equipment.getEquipment(setnum));
				break;
			}
		}

	}
	//装備実メソッド
	public static void setEqu(Character chara,Equipment equ){
		while(true){
			//キャラが何も装備していないとき
			if(chara.getHasEqu()==false){
				chara.setEqu(equ);
				System.out.println("装備しました");
				break;
			}else if(chara.getEqu()!=equ){//キャラが他アイテムを装備中の場合
				System.out.println("装備を入れ替えますか？");
				System.out.println("現在装備:"+chara.getEquName());
				System.out.println("選択中装備:"+equ.getName());
				if(Input.yesOrNo()){
					unSetEqu(chara);
					chara.setEqu(equ);
					System.out.println("装備を入れ替えました");
					break;
				}
			}else if(chara.getEqu()==equ){//キャラが装備している場合
				System.out.println("この装備を外しますか？");
				if(Input.yesOrNo()){
					unSetEqu(chara);
				}else{
					break;
				}
			}
		}
	}



	//装備を外す
	public static void unSetEqu(Character chara){
		System.out.println("装備を外します");
		chara.unsetEqu();
	}

	//キャラクターに戻す
	public static void returnCharacter(Equipment equ){
		System.out.println();
		if(equ.getBaseCharacter()!=null){
			System.out.println("この装備をキャラクタ―に戻します:"+equ.getName());
			Character.getCharacters().add(equ.getBaseCharacter());
			Equipment.Equipments.remove(equ);
		}else{
			System.out.println("この装備はキャラクターにできません!");
		}
	}

	//合成
	public static void synthesisEqu(Equipment equ){
		Equipment base=equ;//ベースアイテム
		System.out.println();
		System.out.println("合成可能な装備一覧");
		List<Equipment> synthesisList=new ArrayList<>();
		synthesisList=EquipmentDisplay.equSimpleList(equ);
		System.out.print("素材にする装備No.を選んでください>>>");
		synth:while(true){
			int mat=Input.inputOutofBounse(synthesisList);
			if(mat<0){
				break synth;//0バック
			}else{
			Equipment material=synthesisList.get(mat);//素材アイテム定義
			synthesisEqu(base,material);//合成
			break synth;
			}
		}
	}

	public static void synthesisEqu(Equipment base,Equipment material){
		System.out.printf("%-4s|%-8s|%-8s|%-8s\n","","ベース","素材","合成結果");
		System.out.printf("%-4s|%-6d|%-6d|%-6d\n","HP",base.getHp(),material.getHp(),(base.getHp()+(material.getHp()/2)));
		System.out.printf("%-4s|%-6d|%-6d|%-6d\n","攻撃",base.getAttack(),material.getAttack(),(base.getAttack()+(material.getAttack()/2)));
		System.out.printf("%-4s|%-6d|%-6d|%-6d\n","回復",base.getHeal(),material.getHeal(),(base.getHeal()+(material.getHeal()/2)));
		System.out.printf("%-6s|%-6d|%-6d|%-6d\n","レア",base.getRare(),material.getRare(),(base.getRare()+1));
		System.out.println("この合成を行いますか？");
		if(Input.yesOrNo()){
			base.setHp((base.getHp()+(material.getHp()/2)));
			base.setAttack((base.getAttack()+(material.getAttack()/2)));
			base.setHeal((base.getHeal()+(material.getHeal()/2)));
			base.setRare((base.getRare()+1));
			Equipment.getEquipments().remove(material);
			System.out.println("合成を行いました");
		}
	}
}
