package Equipments;


import Characters.Character;
import main.Input;

public class EquipmentMenu {
	boolean search;//再帰用サーチフラグ
	//各インスタンス
	EquipmentCreates createEqu=new EquipmentCreates();
	EquipmentDisplay displayEqu=new EquipmentDisplay();
	EquipmentFixes fixEqu=new EquipmentFixes();

	//装備メニュー
	public void equipmentMenu(){
		//デバッグ用装備作成
		if(Character.getCharacters().isEmpty()==false){
			Equipment.DebugEquMaker();
		}
		//ループ
		System.out.println();
		equMenu:while(true){
			System.out.println("装備メニュー");
			System.out.println("装備作成:1");
			System.out.println("装備一覧:2");
			System.out.println("装備させる:3");
			System.out.print("(0)で戻る>>>>>");
			switch(Input.inputNum()){
				case 0:
					//メインメニューに戻る
					break equMenu;
				case 1:
					createEqu.EquCreate();
					break;
				case 2:
					search=true;
					displayEqu.equListDisp();
					break;
				case 3:
					fixEqu.setEqu();
					break;
			}
		}
	}
}
