package Characters;

import main.Input;

public class CharacterFixes {

	//ボーナス分配メニュー
	public void statusBonusMenu(Character chara){
		System.out.println();
		System.out.println("分配:1");
		System.out.println("リセット:2");
		System.out.print("0で戻る>>>");
		bonus:while(true){
			switch(Input.inputNum()){
			case 0:
				//戻る
				break bonus;
			case 1:
				//ボーナス分配
				statusBonusDestribution(chara);
				break bonus;
			case 2:
				//ボーナスリセット
				statusBonusReset(chara);
				break bonus;
			default:
				//ループ
				System.out.println("再入力");
			}
		}
	}

	//ボーナス分配
	public void statusBonusDestribution(Character chara){
		System.out.println();
		while(true){
			//各ボーナス値入力
			System.out.print("HPボーナスを決定(上限30)>");
			int newHpBonus=Input.inputNum(chara.getMaxBonus());
			System.out.print("攻撃ボーナスを決定(上限30)>");
			int newAttackBonus=Input.inputNum(chara.getMaxBonus());
			System.out.print("回復ボーナスを決定(上限30)>");
			int newHealBonus=Input.inputNum(chara.getMaxBonus());
			//再確認表示
			System.out.println("これでよろしいですか？");
			System.out.println("HP"+newHpBonus);
			System.out.println("攻撃"+newAttackBonus);
			System.out.println("回復"+newHealBonus);
			System.out.print(">>>>>");
			if(Input.yesOrNo()){
				//処理
				chara.setHpBonus(newHpBonus);
				chara.setAttackBonus(newAttackBonus);
				chara.setHealBonus(newHealBonus);
				System.out.println("ボーナスptを分配しました!");
				break;
			}
		}
	}

	public void statusBonusReset(Character chara){
		System.out.println("ボーナスをリセットしますか？");
		System.out.println("(リセットしても使用したボーナス値は返ってきません)");
		if(Input.yesOrNo()){
			chara.setHpBonus(0);
			chara.setAttackBonus(0);
			chara.setHealBonus(0);
		}
	}

	//名前修正
	public void charaNameChanger(Character chara){
		System.out.println();
		System.out.print(chara.getName()+"の変更後の名前>>>");
		String newName=Input.inputString();
		System.out.println(chara.getName()+"の名前を"+newName+"に変更しました");
		chara.setName(newName);
	}
}
