package Equipments;

import java.util.ArrayList;
import java.util.List;

import main.Input;

public class EquipmentDisplay {
	boolean search;//再帰用フラグ

	//装備一覧表示
	public void equListDisp(){
		System.out.println();
		search=true;//再帰用
		if(search==true){
			search:while(hasEquipment()==true){//装備有無確認
				System.out.println("一覧を表示します");
				equSimpleList();//装備一覧簡易表示
				System.out.println("表示したい装備No.を入力してください");
				System.out.print("(0)で戻る >>>");
				System.out.println();
				int searchNum=Input.inputOutofBounse(Equipment.getEquipments());
				if(searchNum<0){
					//0バック
					search=false;
					break search;
				}else{
					//装備詳細表示
					equDetealDisp(Equipment.getEquipment(searchNum));
					//装備詳細メニュー呼び出し
					equDetealCommands(Equipment.getEquipment(searchNum));
				}
			}
		}
	}
	//装備単体簡易表示
	public static void equSimpleDisp(Equipment equ,List<Equipment> list){
		System.out.printf("%-6s\t","NO."+(list.indexOf(equ)+1));
		System.out.printf("%-12s",equ.getName());
		System.out.printf("%-2s\t","★"+equ.getRare());
		System.out.println();
	}
	//装備一覧簡易表示
	public static void equSimpleList(){
		System.out.println();
		for(Equipment equ:Equipment.getEquipments()){
			equSimpleDisp(equ,Equipment.getEquipments());
		}
	}
	//装備一覧簡易表示(属性フィルタ)
	public static List<Equipment> equSimpleList(Equipment equ){
		System.out.println();
		List<Equipment> ellementList=new ArrayList<>();
		System.out.printf("%-6s\t","No.");
		System.out.printf("%-2s\t\t","名前");
		System.out.printf("%-2s\t","属性");
		System.out.println();
		for(Equipment list:Equipment.getEquipments()){
			//属性一致&&同一インスタンスでない場合
			if(list.getEllement().equals(equ.getEllement()) && list!=equ){
				ellementList.add(list);
			}
		}
		//表示
		for(Equipment e:ellementList){
			EquipmentDisplay.equSimpleDisp(e,ellementList);
		}
		return ellementList;
	}
	//装備詳細表示
	public void equDetealDisp(Equipment equ){
		System.out.println();
		System.out.printf("%s%d\n","No.",Equipment.getEquipments().indexOf(equ)+1);
		System.out.printf("%-8s\t%s\n","名前",equ.getName());
		System.out.printf("%-4s\t%d\n","HP",equ.getHp());
		System.out.printf("%-6s\t%d\n","攻撃力",equ.getAttack());
		System.out.printf("%-6s\t%d\n","回復力",equ.getHeal());
		System.out.printf("%-2s\t\t%s\n","属性",equ.getEllement());
		System.out.printf("%-6s\t%s\n","装備キャラ",equ.getCharaName());
		System.out.println();
	}
	//装備所持チェック
	public static boolean hasEquipment(){
		if(Equipment.getEquipments().isEmpty()==true){
			System.out.println("装備品がありません");
			System.out.println();
			return false;
		}else{
			return true;
		}
	}
	//装備選択時詳細コマンド
	public void equDetealCommands(Equipment equ){
		System.out.println("<<<コマンド>>>");
		System.out.println("装備:1");
		System.out.println("合成:2");
		System.out.println("キャラクターに戻す:3");
		System.out.print(">>>>>>>>>>");
		command:while(true){
			switch(Input.inputNum()){
			case 0:
				break command;
			case 1:
				EquipmentFixes.setEqu(equ);//装備メソッド
				break command;
			case 2:
				EquipmentFixes.synthesisEqu(equ);//合成メソッド
				break command;
			case 3:
				EquipmentFixes.returnCharacter(equ);//キャラに戻すメソッド
				break command;
			default:
				System.out.print("再入力>>>");
			}
		}
	}
}
