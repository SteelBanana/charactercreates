package Characters;

import java.util.List;

import Equipments.EquipmentFixes;
import main.Input;

public class CharacterDisplay {
	boolean search;//サーチフラグ(再帰時表示処理用)
	CharacterFixes charafix=new CharacterFixes();

	//キャラクター一覧表示
	public void charaListDisp(){
		System.out.println();
		search=true;//キャラメニューから呼び出された用
		if(search==true){
			search:while(hasCharaList()==true){//所持チェック
				System.out.println("一覧を表示します");
				charaSimpleList();//簡易リスト表示
				System.out.println("表示したいキャラクターNo.を入力してください");
				System.out.print("(0)で戻る >>>");
				//No.入力で呼び出し
				int searchNum=Input.inputOutofBounse(Character.getCharacters());
				if(searchNum<0){
					//0入力でキャラメニューに戻る処理
					search=false;
					break search;
				}else{
					charaDetealDisp(Character.getCharacter(searchNum));//詳細表示
					charaDetealCommands(Character.getCharacter(searchNum));//詳細コマンド
					break search;
				}
			}
		}
	}

	//キャラクター詳細表示
	public static void charaDetealDisp(Character chara){
		System.out.println();
		System.out.printf("%s%d\n","No.",Character.getCharacters().indexOf(chara)+1);
		System.out.printf("%-8s\t%s\t%s\n","名前",chara.getName(),"ボーナス");
		System.out.printf("%-4s\t%d\t\t%d/%d\n","HP(+)",chara.getHp(),chara.getHpBonus(),Character.getMaxBonus());
		System.out.printf("%-6s\t%d\t\t%d/%d\n","攻撃力(+)",chara.getAttack(),chara.getAttackBonus(),Character.getMaxBonus());
		System.out.printf("%-6s\t%d\t\t%d/%d\n","回復力(+)",chara.getHeal(),chara.getHealBonus(),chara.getMaxBonus());
		System.out.printf("%-2s\t\t%s\n","属性",chara.getEllement());
		System.out.printf("%-2s\t\t%s\n","装備",chara.getEquName());
		System.out.println();
	}

	//キャラクター一覧簡易表示
	public static void charaSimpleList(){
		System.out.println();
		//見出し表示
		System.out.printf("%-6s\t","No.");
		System.out.printf("%-2s\t\t","名前");
		System.out.printf("%-2s\t","属性");
		System.out.println();
		//キャラList呼び出して簡易表示ループ
		for(Character chara:Character.getCharacters()){
			charaSimpleDisp(chara,Character.getCharacters());
		}
	}

	//キャラクター単体簡易表示
	public static void charaSimpleDisp(Character chara,List<Character> list){
		System.out.printf("%-6s\t","NO."+(list.indexOf(chara)+1));
		System.out.printf("%-12s",chara.getName());
		System.out.printf("%-2s\t",chara.getEllement());
		System.out.println();
	}

	//キャラクター詳細表示時メニュー
	public void charaDetealCommands(Character chara){
		System.out.println("<<<コマンド>>>");
		command:while(true){
			System.out.println("装備:1");
			System.out.println("名前変更:2");
			System.out.println("ボーナス分配:3");
			System.out.print(">>>>>>>>>>");
			switch(Input.inputNum()){
			case 0:
				//0入力で戻る
				break command;
			case 1:
				EquipmentFixes.setEqu(chara);
				break command;
			case 2:
				charafix.charaNameChanger(chara);
				break command;
			case 3:
				charafix.statusBonusMenu(chara);
				break command;
			default:
				continue;
			}
		}
	}

	//キャラクター所持チェック
	public static boolean hasCharaList(){
			if(Character.getCharacters().isEmpty()==true){
				System.out.println("キャラクターがいません");
				System.out.println();
				return false;
			}else{
				return true;
			}
		}
}
