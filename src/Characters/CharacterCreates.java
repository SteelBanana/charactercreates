package Characters;
import java.util.Random;

import main.Input;

public class CharacterCreates {
	static int autoCount=0;//自動作成累計数
	static final int AutoLimit=20;//自動作成数上限
	Character chara=new Character();//キャラクター空インスタンス

	//キャラクター手動生成
	public void charaHandMake(){
		//各ステータス乱数生成
		int bonus=new Random().nextInt(21)+10;//ボーナス
		int hpAddRandom=new Random().nextInt(11)+110;//HP
		int attackAddRandom=new Random().nextInt(11)+10;//攻撃
		int healAddRandom=new Random().nextInt(11)+10;//回復
		int ellementRandom=new Random().nextInt(5);//属性
		//表示
		System.out.println();
		System.out.println("基礎HP:"+hpAddRandom);
		System.out.println("基礎攻撃力:"+attackAddRandom);
		System.out.println("基礎回復力:"+healAddRandom);
		System.out.println("属性:"+Character.getEllement(ellementRandom));
		System.out.println("ステータスボーナス:"+bonus);
		System.out.println("このキャラクターを作成しますか？");
		if(Input.yesOrNo()){
			System.out.println("キャラクターに名前を付けてください");
			String name=Input.inputString();//名前入力
			//実作成メソッド呼び出し
			chara=charaMaker(name,hpAddRandom,attackAddRandom
					,healAddRandom,ellementRandom,bonus);
			System.out.println("キャラクターを作成しました!");
			//簡易表示呼び出し
			CharacterDisplay.charaSimpleDisp(chara,Character.getCharacters());
		}
	}
	//キャラクター自動生成
	public void autoCharaMake(){
		System.out.println();
		System.out.println("(残り作成可能数:"+(AutoLimit-autoCount)+")");
		while(true){
			System.out.println("作成するキャラクター数を入力");
			int num=Input.inputNum();//作成数
			//作成数チェック処理
			if(autoCount+num>AutoLimit){
				System.out.println("許可されていません");
				break;
			}else{
				for(int i=0; i<num;i++){
				String name="Name"+(autoCount+1);
				//各ステータス乱数生成
				int bonus=new Random().nextInt(21)+15;
				int hpAddRandom=new Random().nextInt(11)+115;
				int attackAddRandom=new Random().nextInt(11)+15;
				int healAddRandom=new Random().nextInt(11)+15;
				int ellementRandom=new Random().nextInt(5);
				//実作成メソッド
				chara=charaMaker(name,hpAddRandom,attackAddRandom
						,healAddRandom,ellementRandom,bonus);
				System.out.println(name+"が生まれました!");
				//表示
				CharacterDisplay.charaSimpleDisp(chara,Character.getCharacters());
				autoCount++;//累計作成数加算
				}
			}
			break;
		}
	}

	//キャラクター作成実メソッド
	public static Character charaMaker(String name,int hp, int attack, int heal,
							int ellement,int remnantBonus){
		Character chara=new Character();//キャラクターインスタンス
		chara.setName(name);		//各
		chara.setBaseHp(hp);		//要
		chara.setBaseAttack(attack);//素
		chara.setBaseHeal(heal);	//設
		chara.setEllement(ellement);//定
		Character.addRemnantBonus(remnantBonus);//ステータスボーナス加算
		Character.getCharacters().add(chara);//キャラListに追加
		return chara;
	}
}
