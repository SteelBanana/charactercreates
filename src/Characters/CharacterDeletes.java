package Characters;

import main.Input;
public class CharacterDeletes {
	boolean search=true;//再帰表示用フラグ

	//キャラクター削除メニュー
	public void charaDeleteMenu(){
		System.out.println();
		delete:while(search=true){
			while(CharacterDisplay.hasCharaList()==true){
				//キャラ一覧簡易表示
				CharacterDisplay.charaSimpleList();
				System.out.println("削除したいキャラクターNo.を入力してださい");
				System.out.print("(0で戻る)");
				System.out.print(">>>>>>");
				int searchNum=Input.inputOutofBounse(Character.getCharacters());
				if(searchNum<0){
					//0入力で戻る
					search=false;
					break delete;
				}else{
					//詳細表示
					Character chara=Character.getCharacter(searchNum);
					CharacterDisplay.charaDetealDisp(chara);
					System.out.println("このキャラクターを本当に削除しますか？");
					System.out.print("する:y しない:n >>>>>>");
					if(Input.yesOrNo()){
						charaDeleater(chara);
						System.out.println("削除しました");
						System.out.println();
						break;
					}
				}
			}
		}
	}
	//キャラクター削除実メソッド
	public static void charaDeleater(Character chara){
		Character.getCharacters().remove(Character.getCharacters().indexOf(chara));
	}
}
