package Equipments;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import Characters.Character;

public class Equipment {

	String name;//名前
	private int hp;//HP
	private int attack;//攻撃力
	private int heal;//回復力
	static private  List<String> EllementName=Arrays.asList("無垢なる","火の","水の","光の","闇の");//属性実名リスト
	private String Ellement;//属性実名
	static List<Equipment> Equipments=new ArrayList<>();//装備リスト
	private int Rare;//レア度
	private Character baseCharacter;//素体キャラクター
	private boolean setable=true;//装備可能フラグ
	private Character settingCharacter;//装備中キャラクター

	//名前
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public void setName(int ellement,String name){
		setName(EllementName.get(ellement)+name+"カード");
	}

	//HP系
	public int getHp() {
		return hp;
	}
	public void setHp(int hp) {
		this.hp = hp;
	}

	//攻撃系
	public int getAttack() {
		return attack;
	}
	public void setAttack(int attack) {
		this.attack = attack;
	}

	//回復系
	public int getHeal() {
		return heal;
	}
	public void setHeal(int heal) {
		this.heal = heal;
	}

	//属性系
	public String getEllement() {
		return Ellement;
	}

	public void setEllement(String  ellement) {
		Ellement = ellement;
	}

	//レア系
	public int getRare() {
		return Rare;
	}
	public void setRare(int rare) {
		Rare = rare;
	}

	//その他
	//装備リスト取得
	public static List<Equipment> getEquipments() {
		return Equipments;
	}
	//装備取得(全要素から)
	public static Equipment getEquipment(int num){
		return getEquipments().get(num);
	}
	//ベースキャラクター取得
	public Character getBaseCharacter() {
		return baseCharacter;
	}
	//ベースキャラクターセット
	public void setBaseCharacter(Character baseCharacter) {
		this.baseCharacter = baseCharacter;
	}

	//装備系
	//装備フラグセット
	public void setableEqu(boolean able){
		this.setable=able;
	}
	//装備中キャラクターセット
	public void setSettingCharacter(Character chara){
		this.settingCharacter=chara;
	}
	//装備中キャラクター取得
	public Character getSettingCharacter(){
		return this.settingCharacter;
	}
	//装備中キャラクター名取得
	public String getCharaName(){
		if(this.setable==true){
			return "なし";
		}else{
			return this.settingCharacter.getName();
		}
	}

	//デバッグ用
	public static void DebugEquMaker(){
		for(int i=Character.getCharacters().size()-6 ; i>=0; i-- ){
			Character chara=Character.getCharacter(i);
			EquipmentCreates.equMaker(chara);

		}
	}
}
