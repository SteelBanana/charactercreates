package Equipments;

import Characters.Character;
import Characters.CharacterDeletes;
import Characters.CharacterDisplay;
import main.Input;

public class EquipmentCreates {

	Equipment equ=new Equipment();
	//装備作成
	public void EquCreate(){
		System.out.println();
		System.out.println("装備を作成します");
		//キャラ有無確認
		create:if(Characters.CharacterDisplay.hasCharaList()){
			while(true){
				System.out.println("素材にするキャラクターを選んでください");
				System.out.println("(0)を入力すると装備メニューへ戻る");
				CharacterDisplay.charaSimpleList();//キャラ簡易表示
				System.out.print(">>>>>>No.");
				int num=Input.inputOutofBounse(Character.getCharacters());
				Character chara=new Character();
				if(num<0){
					break create;//0バック
				}else{
					chara=Character.getCharacter(num);//キャラ取得
					CharacterDisplay.charaDetealDisp(chara);//キャラ詳細表示
					System.out.println("このキャラクターを装備品にしますか？");
					System.out.println("※装備品にしたキャラクターは後で復元できます");
					if(Input.yesOrNo()){
						equ=equMaker(chara);//装備作成実メソッド
						System.out.println("装備品を作成しました");
						//装備簡易表示
						EquipmentDisplay.equSimpleDisp(equ,Equipment.getEquipments());
						break;
					}
				}
			}
		}
	}

	//装備作成実処理
	public static Equipment equMaker(Character chara){
		Equipment equ=new Equipment();//インスタンス作成
		//各要素セット
		equ.setName(chara.getEllement(chara),chara.getName());
		equ.setHp(chara.getbaseHp()/5);
		equ.setAttack(chara.getBaseAttack()/5);
		equ.setHeal(chara.getHeal()/5);
		equ.setEllement(chara.getEllement());
		equ.setRare(1);//レア
		equ.setBaseCharacter(chara);//ベースキャラクタ
		Equipment.getEquipments().add(equ);//装備追加
		CharacterDeletes.charaDeleater(chara);//キャラ削除
		return equ;
	}

}
