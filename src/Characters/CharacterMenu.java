package Characters;
import main.Input;

public class CharacterMenu {
	//各クラスインスタンス
	CharacterCreates charaCreate=new CharacterCreates();
	CharacterDisplay charaDisp=new CharacterDisplay();
	CharacterDeletes charaDelete=new CharacterDeletes();

	//キャラクターメインメニュー
		public void charaMenu(){
			TopMenu:while(true){
				System.out.println("キャラクターメニュー");
				System.out.println("キャラクター任意作成:1");
				System.out.println("キャラクター自動作成:2");
				System.out.println("キャラクター一覧:3");
				System.out.println("キャラクター消去:4");
				System.out.print("(0)で戻る");
				System.out.print(">>>>>>");

				charaMenu:while(true){
				switch(Input.inputNum()){
				case 0:
					//メインメニューに戻る(main.Mainに戻る)
					break TopMenu;
				case 1:
					//手動キャラ作成
					charaCreate.charaHandMake();
					break charaMenu;
				case 2:
					//自動キャラ作成
					charaCreate.autoCharaMake();
					break charaMenu;
				case 3:
					//キャラ一覧表示
					charaDisp.charaListDisp();
					break charaMenu;
				case 4:
					//キャラ消去
					charaDelete.charaDeleteMenu();
					break charaMenu;
				default:
					System.out.print("再入力>>>");
					break;
				}
			}
		}
	}
}
