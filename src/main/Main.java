package main;
import Characters.Character;
import Characters.CharacterMenu;
import Equipments.EquipmentMenu;

public class Main {
	static CharacterMenu characters=new CharacterMenu();//キャラメニュー呼び出しインスタンス
	static EquipmentMenu equipments=new EquipmentMenu();//装備メニュー呼び出しインスタンス

	public static void main(String[] args) {

		Character.DebugCharacters();//デバッグ用自動キャラ生成

		while(true){
			System.out.println("メインメニュー");
			System.out.println("キャラクターメニュー:1");
			System.out.println("装備メニュー:2");
			System.out.print(">>>>>>>>>>>>");
			menuSwitchng();//実メニュー処理呼び出し
		}
	}

	//メニュー実メソッド
		public static void menuSwitchng(){
			mainMenu:while(true){
				switch(Input.inputNum()){
				case 1:
					System.out.println();
					characters.charaMenu();
					break mainMenu;
				case 2:
					System.out.println();
					equipments.equipmentMenu();
					break mainMenu;
				default:
					System.out.print("再入力>>>>");
					break;
				}
			}
		}

}
